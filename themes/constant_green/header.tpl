<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<title>The Constant Grower: Horticulture Management</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />

        <link type="text/css" href="{$themeurl}/css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />    
        <script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">
if((navigator.userAgent.match(/iPad/i))) {
document.write("<link type=\"text\/css\" rel=\"stylesheet\" media=\"all\" href=\"themes/constant_green/css/ipad.css\" charset=\"utf-8\" \/>");
document.write("<meta name=\"viewport\" content=width=1024px, minimum-scale=1.0, maximum-scale=1.0 \/>");
}
else if(navigator.userAgent.match(/iPhone/i)) {
document.write("<link type=\"text\/css\" rel=\"stylesheet\" media=\"all\" href=\"themes/constant_green/css/iphone.css\" charset=\"utf-8\" \/>");
document.write("<meta name=\"viewport\" content=width=480px, minimum-scale=1.0, maximum-scale=1.0 \/>");
}
else
{
document.write("<link type=\"text\/css\" rel=\"stylesheet\" href=\"themes/constant_green/css/common.css\" \/>");
}
</script>
        <script type="text/javascript" src="js/setup_wizard.js"></script>
        <script type="text/javascript" src="js/stepxstep.js"></script> 
        <script type="text/javascript" src="{$themeurl}/js/forms.js"></script>
        <script type="text/javascript" src="{$themeurl}/js/plant_tabs.js"></script>
        <link rel="stylesheet" href="{$themeurl}/css/forms.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="{$themeurl}/css/C_green.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="{$themeurl}/css/pagination.css" type="text/css" media="screen" />  
</head>

<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF" onload="prettyForms()">


<table border="0" width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
        
<!-- START HEADER HTML -->
<table id="Table_01" width="100%" height="217" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="928" height="98">
            <a href="index.php">
            <img src="themes/constant_green/images/header_logo_menu/header_logo_menu_LOGO.png" width="772" height="98" alt="" border="0" /></a></td>
        <td width="972" height="145" rowspan="2" valign="top">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </td>
        <td width="1" height="217" rowspan="3">
            <img src="themes/constant_green/images/header_logo_menu/header_logo_menu_RT-TBL-BG.png" width="1" height="217" alt="" /></td>
    </tr>
    <tr>
        <td width="928" height="47" valign="top">
        <!--START AD SPACE-->
        <!--END AD SPACE-->
        </td>
    </tr>
    <tr>
        <td width="928" height="72" background="themes/constant_green/images/header_logo_menu/header_logo_menu_NAVMENU_BG.png">
<!-- START NAV MENU -->
<table id="Table_01" width="979" height="58" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="6" height="58" rowspan="3">
            <img src="themes/constant_green/images/header_menu_transparent/header_nav_menu_ON_trans_leftcrnr.png" width="6" height="58" alt="" /></td>
        <td width="968" height="5" colspan="7">
            <img src="themes/constant_green/images/header_menu_transparent/header_nav_menu_ON_trans_top.png" width="968" height="5" alt="" /></td>
        <td width="5" height="58" rowspan="3">
            <img src="themes/constant_green/images/header_menu_transparent/header_nav_menu_ON_trans_rtcrnr.png" width="5" height="58" alt="" /></td>
    </tr>
    <tr>
        <td width="108" height="48">
        {if $SCRIPT_NAME eq '/dashboard.php'}
        <a href="dashboard.php"
                onmouseover="window.status='Dash Board';  return true;"
                onmouseout="window.status='';  return true;">
                <img src="themes/constant_green/images/header_menu_off/header_nav_menu_OFF_HOME.png" width="108" height="48" border="0" alt="Dashboard" /></a>
        {else}
        <a href="dashboard.php"
                onmouseover="window.status='Dash Board';  return true;"
                onmouseout="window.status='';  return true;">
                <img src="themes/constant_green/images/header_menu_transparent/header_nav_menu_ON_HOME.png" width="108" height="48" border="0" alt="Dashboard" /></a>
        {/if}
        </td>
        <td width="129" height="48">
        {if $SCRIPT_NAME eq '/plants.php'} 
            <a href="plants.php"
                onmouseover="window.status='Plants';  return true;"
                onmouseout="window.status='';  return true;">
                <img src="themes/constant_green/images/header_menu_off/header_nav_menu_OFF_PLANTS.png" width="129" height="48" border="0" alt="Plants" /></a>
        {else}
        <a href="plants.php"
                onmouseover="window.status='Plants';  return true;"
                onmouseout="window.status='';  return true;">
                <img src="themes/constant_green/images/header_menu_transparent/header_nav_menu_ON_PLANTS.png" width="129" height="48" border="0" alt="Plants" /></a>
        {/if}
        </td>
        <td width="123" height="48">
        {if $SCRIPT_NAME eq '/grow_rooms.php'} 
            <a href="grow_rooms.php"
                onmouseover="window.status='Grow Rooms';  return true;"
                onmouseout="window.status='';  return true;">
                <img src="themes/constant_green/images/header_menu_off/header_nav_menu_OFF_ROOMS.png" width="123" height="48" border="0" alt="Growrooms" /></a>
        
        {else}
        <a href="grow_rooms.php"
                onmouseover="window.status='Grow Rooms';  return true;"
                onmouseout="window.status='';  return true;">
                <img src="themes/constant_green/images/header_menu_transparent/header_nav_menu_ON_ROOMS.png" width="123" height="48" border="0" alt="Growrooms" /></a>  
        {/if}        
        </td>
        <td width="143" height="48">
        {if $SCRIPT_NAME eq '/cloning.php'}
            <a href="cloning.php"
                onmouseover="window.status='Cloning';  return true;"
                onmouseout="window.status='';  return true;">
                <img src="themes/constant_green/images/header_menu_off/header_nav_menu_OFF_CLONING.png" width="143" height="48" border="0" alt="Cloning" /></a>
        {else}
        <a href="cloning.php"
                onmouseover="window.status='Cloning';  return true;"
                onmouseout="window.status='';  return true;">
                <img src="themes/constant_green/images/header_menu_transparent/header_nav_menu_ON_CLONING.png" width="143" height="48" border="0" alt="Cloning" /></a>
        {/if}
        </td>
        <td width="163" height="48">
        {if $SCRIPT_NAME eq '/patients.php'} 
            <a href="patients.php"
                onmouseover="window.status='Patients ';  return true;"
                onmouseout="window.status='';  return true;">
                <img src="themes/constant_green/images/header_menu_off/header_nav_menu_OFF_PATIENTS.png" width="163" height="48" border="0" alt="Patients" /></a>
        {else}
            <a href="patients.php"
                onmouseover="window.status='Patients ';  return true;"
                onmouseout="window.status='';  return true;">
                <img src="themes/constant_green/images/header_menu_transparent/header_nav_menu_ON_PATIENTS.png" width="163" height="48" border="0" alt="Patients" /></a>
        {/if}        
        </td>
        <td width="145" height="48">
        {if $SCRIPT_NAME eq '/finance.php'} 
            <a href="finance.php"
                onmouseover="window.status='Finance';  return true;"
                onmouseout="window.status='';  return true;">
                <img src="themes/constant_green/images/header_menu_off/header_nav_menu_OFF_FINANCE.png" width="145" height="48" border="0" alt="Finance" /></a>
         {else}
            <a href="finance.php"
                onmouseover="window.status='Finance';  return true;"
                onmouseout="window.status='';  return true;">
                <img src="themes/constant_green/images/header_menu_transparent/header_nav_menu_ON_FINANCE.png" width="145" height="48" border="0" alt="Finance" /></a>           
         {/if}       
        </td>
        <td width="157" height="48">
        {if $SCRIPT_NAME eq '/options.php'} 
            <a href="options.php"
                onmouseover="window.status='Options';  return true;"
                onmouseout="window.status='';  return true;">
                <img src="themes/constant_green/images/header_menu_off/header_nav_menu_OFF_OPTIONS.png" width="157" height="48" border="0" alt="Options" /></a>
        {else}
            <a href="options.php"
                onmouseover="window.status='Options';  return true;"
                onmouseout="window.status='';  return true;">
                <img src="themes/constant_green/images/header_menu_transparent/header_nav_menu_ON_OPTIONS.png" width="157" height="48" border="0" alt="Options" /></a>  
        {/if}
        </td>
    </tr>
    <tr>
        <td width="968" height="5" colspan="7">
            <img src="themes/constant_green/images/header_menu_transparent/header_nav_menu_ON_trans_bottom.png" width="968" height="5" alt="" /></td>
    </tr>
</table>
<!-- END NAV MENU -->        
        
        
        
        
        
        
        </td>
        <td width="972" height="72" background="themes/constant_green/images/header_logo_menu/header_logo_menu_NAVMENU_BG.png">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>  
<!-- END HEADER -->
</td>
    </tr>
    <tr>
        <td valign="top">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td width="260" valign="top" background="themes/constant_green/images/left_menu/left_menu_bottom.png">
                
                
               
<!-- start left nav menu html-->
<table border="0" width="260" cellspacing="0" cellpadding="0" height="100%">
    <tr>
        <td valign="top" background="themes/constant_green/images/left_menu/left_menu_bottom.png">
<!-- START LEFT MENU -->
<table id="Table_01" width="260" height="408" border="0" cellpadding="0" cellspacing="0" >
    <tr>
        <td width="231" height="39" colspan="3">
            <img src="themes/constant_green/images/left_menu/PAGE_LAYOUT_MOCKUP_headernav.png" width="231" height="39" alt="" /></td>
        <td width="29" height="39">
            <img src="themes/constant_green/images/left_menu/PAGE_LAYOUT_MOCKUP_pagecorner.png" width="29" height="39" alt="" /></td>
    </tr>
    <tr>
        <td width="11" height="311" background="themes/constant_green/images/left_menu/PAGE_LAYOUT_MOCKUP_lftside.png">
            &nbsp;</td>
        <td width="213" height="311" bgcolor="#FFFFFF" valign="top">

<!--START MENU ITEM LISTINGS -->
{include file="$theme_dir/home_links.tpl"}
{include file="$theme_dir/plant_links.tpl"}
{include file="$theme_dir/gr_links.tpl"} 
{include file="$theme_dir/cloning_links.tpl"} 
{include file="$theme_dir/patient_links.tpl"} 
{include file="$theme_dir/finance_links.tpl"}
{include file="$theme_dir/options_links.tpl"}  
<!--END MENU ITEM LISTINGS -->

    
        </td>
        <td width="7" height="311" background="themes/constant_green/images/left_menu/PAGE_LAYOUT_MOCKUP_riteside.png">
            &nbsp;</td>
        <td width="29" height="333" rowspan="2" class="nav_menu_rtside" background="themes/constant_green/images/left_menu/PAGE_LAYOUT_MOCKUP_pagecorner2.png">
            &nbsp;</td>
    </tr>
    <tr>
        <td width="231" height="22" colspan="3" background="themes/constant_green/images/left_menu/PAGE_LAYOUT_MOCKUP_footer.png">
            &nbsp;</td>
    </tr>
    <tr>
        <td width="260" height="100%" colspan="4" bgcolor="#84bc02" background="themes/constant_green/images/left_menu/left_menu_bottom.png">
            &nbsp;</td>
    </tr>
</table>
<!-- END LEFT MENU -->
        </td>
    </tr>
</table>
<!--end left nav html -->

                
               </td>
                <td width="74%" valign="top">
                <!--START THE PRIMARY PAGE CONTENT THAT GETS CALLED -->