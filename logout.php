<?php
require_once(dirname(__FILE__).'/lib/config.inc.php');
require_once(dirname(__FILE__).'/lib/mysql.inc.php'); 
session_start();

;

if($_SESSION['LOGIN_STATUS'] != "OK"){
    session_unset();
    session_destroy();
    echo 'You are not logged in, redirecting<meta http-equiv="Refresh" content="1; URL=login.php">';
}else{
include(dirname(__FILE__).'/header.php');
if(session_destroy()){

    echo 'Successfully logged out, redirecting in 2 seconds please stand by.';
    echo ' <meta http-equiv="Refresh" content="1; URL=login.php">';
}


include(dirname(__FILE__).'/footer.php');
}
?>