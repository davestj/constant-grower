<?php
require_once(dirname(__FILE__).'/lib/config.inc.php');

session_start();
if($_SESSION['LOGIN_STATUS'] != "OK"){
    session_unset();
    session_destroy();
    echo 'You are not logged in, redirecting<meta http-equiv="Refresh" content="1; URL=login.php">';
}else{
include(dirname(__FILE__).'/header.php');
 echo '<br>';


include(dirname(__FILE__).'/footer.php');
}
?>