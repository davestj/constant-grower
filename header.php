<?php
include(dirname(__FILE__).'/lib/config.inc.php');
session_start();
include(dirname(__FILE__).'/lib/mysql.inc.php');
include(dirname(__FILE__).'/lib/functions.inc.php');

//SMARTY SETTINGS
require_once('Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = ''.$DOCUMENT_ROOT.'/templates/';
$smarty->compile_dir  = ''.$DOCUMENT_ROOT.'/templates_c/';
$smarty->config_dir   = ''.$DOCUMENT_ROOT.'/configs/';
$smarty->cache_dir    = ''.$DOCUMENT_ROOT.'/cache/';
$smarty->debugging = TRUE;

$smarty->assign('theme_dir',''.$theme_path.'');
$smarty->assign('active_page',''.$PHP_SELF.'');
$smarty->assign('themeurl',$theme_url);
//generate navigation menus for each section

//DEFINE HOME SECTION MENU ITEMS
$home_links_array = array(
                   'My Messages' => 'messages.php?context=inbox',
                   'Monitor'     => 'monitoring.php?context=index',
                   'Latest News' => 'news.php?context=latest',
                   'Links'       => 'links.php?context=recent',
                   'Support'     => 'support.php?context=index',
                   'Calendar'    => 'calendar.php?context=events',
                   'Logout'      => 'logout.php?leave=true');
$smarty->assign('home_links',$home_links_array);

//DEFINE PLANT LINKS ARRAY
$plant_links_array = array(
                   'Active List' => 'plants.php?context=index',
                   'Strain Library'     => 'library.php?context=strains',
                   'Feeding Schedules' => 'scheduling.php?context=feeding',
                   'Reports'            => 'reports_plants.php?context=plants');
$smarty->assign('plant_links',$plant_links_array);

//DEFINE GROW ROOMS LINKS ARRAY
$gr_links_array = array(
                   'Assets' => 'assets.php?context=index',
                   'Grow Rooms'     => 'grow_rooms.php?context=index',
                   'Reports'            => 'reports_gr.php?context=grow_rooms');
$smarty->assign('gr_links',$gr_links_array);

//DEFINE CLONING LINKS ARRAY
$cloning_links_array = array(
                   'Clone List' => 'cloning.php?context=list',
                   'Reports'            => 'reports_cloning.php?context=cloning');
$smarty->assign('cloning_links',$cloning_links_array);


//DEFINE PATIENTS LINKS ARRAY
$patient_links_array = array(
                   'Patient Directory' => 'patients.php?context=index',
                   'Reports'            => 'reports_patients.php?context=patients');
$smarty->assign('patient_links',$patient_links_array);


//DEFINE FINANCE LINKS ARRAY
$finance_links_array = array(
                   'Budgeting' => 'budget.php?context=index',
                   'Reports'            => 'reports_finance.php?context=finance');
$smarty->assign('finance_links',$finance_links_array);

//DEFINE SYSTEM ADMIN/OPTIONS LINKS ARRAY
$options_links_array = array(
                   'System Settings' => 'sys_settings.php?context=index',
                   'User Management' => 'users.php?context=index',
                   'System Reports'  => 'reports_system.php?context=system');
$smarty->assign('options_links',$options_links_array);

//PROCESS POST/GET/SESSION/GLOBAL VARS
$Gcontext   =        $_GET['context'];
$Gprompt    =        $_GET['prompt'];
$Plid       =        $_GET['PLID'];
$Gsort      =        $_GET['sort'];
$Gsort_mode =        $_GET['sort_mode']; 
$Uid        =        $_SESSION['user_id'];
$uname      =        $_SESSION['U_name'];
$Uemail     =        $_SESSION['email'];
$Ualias     =        $_SESSION['alias_name'];
$Authstatus =        $_SESSION['LOGIN_STATUS'];
$cg_mode    =        $_SESSION['GROW_MODE'];


//GRAB SOME QUICK STATS
//PLANTS
$Psql = mysql_query("SELECT COUNT(name) FROM cg_plants");
$Tplant_check = mysql_fetch_array($Psql); 

//GROW ROOMS
$Gsql = mysql_query("SELECT COUNT(ID) FROM cg_rooms") or die(mysql_error());
$gr_check = mysql_fetch_array($Gsql);

//PATIENTS
$Pasql = mysql_query("SELECT COUNT(ID) FROM cg_patients") or die(mysql_error());
$patient_check = mysql_fetch_array($Pasql);

//ASSETS
$Asql = mysql_query("SELECT COUNT(ID) FROM cg_assets") or die(mysql_error());
$asset_check = mysql_fetch_array($Asql);

//CLONES
$Csql = mysql_query("SELECT COUNT(ID) FROM cg_cloning") or die(mysql_error());
$clone_check = mysql_fetch_array($Csql);

//AND LETS SHOW THEM WHAT THEY BEEN WAITING FOR
$smarty->display(''.$theme_path.'/header.tpl');  

?>