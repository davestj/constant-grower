<?php
require_once(dirname(__FILE__).'/lib/config.inc.php');

session_start();
if($_SESSION['LOGIN_STATUS'] != "OK"){
    session_unset();
    session_destroy();
    echo '<html>
<head>
<title>Constant Grower Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
';
include(dirname(__FILE__).'/login.php');
}else{
include(dirname(__FILE__).'/header.php');





if($Tplant_check[0] <  1 && $gr_check[0] < 1 && $asset_check[0] < 1){ 
    $msg = '<strong>Hey!</strong> Looks like this is your first time logging in, would you like to run the <a href="setup_wizard.php?context=step1">setup wizard</a>?<br>
This is highly recomended you do this in order to get a good understanding on how the system works.<br>';
notice_msg($msg,info);
    
}




//echo ''.$Uid.' '.$uname.'Mode: '.$cg_mode.'<br>';


if($Gcontext =="devel"){
echo '
<div class="demo">

<div id="dialog-form" title="Create new user">
    <p class="validateTips">All form fields are required.</p>

    <form>
    <fieldset>
        <label for="name">Name</label>
        <input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" />
        <label for="email">Email</label>

        <input type="text" name="email" id="email" value="" class="text ui-widget-content ui-corner-all" />
        <label for="password">Password</label>
        <input type="password" name="password" id="password" value="" class="text ui-widget-content ui-corner-all" />
    </fieldset>
    </form>
</div>


<div id="users-contain" class="ui-widget">
    <h1>Existing Users:</h1>

    <table id="users" class="ui-widget ui-widget-content">
        <thead>
            <tr class="ui-widget-header ">
                <th>Name</th>
                <th>Email</th>
                <th>Password</th>
            </tr>

        </thead>
        <tbody>
            <tr>
                <td>John Doe</td>
                <td>john.doe@example.com</td>
                <td>johndoe1</td>
            </tr>

        </tbody>
    </table>
</div>
<button id="create-user">Create new user</button>

</div><!-- End demo -->
';
}

include(dirname(__FILE__).'/footer.php');
}
?>