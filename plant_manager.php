<?php
require_once(dirname(__FILE__).'/lib/config.inc.php');

session_start();
if($_SESSION['LOGIN_STATUS'] != "OK"){
    session_unset();
    session_destroy();
    echo 'You are not logged in, redirecting<meta http-equiv="Refresh" content="1; URL=login.php">';
}else{
include(dirname(__FILE__).'/header.php'); 
echo '<br>';
$smarty->assign('page_title','Plant Manager');

if($Gcontext == 'Pedit'){

//get mysql data
$query = mysql_query("SELECT * FROM cg_plants WHERE ID = '$Plid'");
  $result = mysql_fetch_array($query);
include('./lib/plant_query.php');

$msg = '<strong>Editing plant record #'.$Plid.'</strong>';
notice_msg($msg,info);

echo '
        <div id="edit_plant_tabs">

            <ul>
                <li><a href="#tabs-1">General</a></li>
                <li><a href="#tabs-2">Health</a></li>
                <li><a href="#tabs-3">Schedules</a></li>
            </ul>
            <div id="tabs-1">';
               
//GENERAL PLANT OPTIONS
$smarty->display(''.$theme_path.'/contentbg_header.tpl'); 
    echo'General plant options';
$smarty->display(''.$theme_path.'/contentbg_footer.tpl');



//PLANT HEALTH OPTIONS
echo'</div><div id="tabs-2">';
$smarty->display(''.$theme_path.'/contentbg_header.tpl');
echo 'Plant Health';
$smarty->display(''.$theme_path.'/contentbg_footer.tpl');
   
 //FEEDING SCHEDULES          
echo '</div><div id="tabs-3">';
$smarty->display(''.$theme_path.'/contentbg_header.tpl');
echo 'Feeding Schedules';
$smarty->display(''.$theme_path.'/contentbg_footer.tpl'); 
            
            
            echo'</div>
        </div>
';







}



if($Gcontext == 'Padd'){
$msg = '<strong>Adding new plant.</strong><br>Note: fiels marked with a * indicate mandatory settings.';
notice_msg($msg,info);
$smarty->display(''.$theme_path.'/contentbg_header.tpl'); 


//generate the form now.

//mysql_generate_vars("cg_plants","CGplants_");
//mysql_formgen_debug("cg_plants");
//construct our form from sql query aliasing
$QUERY = "SELECT plant_tagID AS 'Tag Id#',
name AS 'Name',
age AS Age,
height AS 'Height in inches',
location AS 'Location',
generation AS 'Generation',
status AS 'Status',
image AS 'Picture',
notes AS 'Notes',
dna_inheritance AS 'DNA Inheritance',
dna_gender AS 'DNA Gender',
plant_strain AS 'Strain' FROM cg_plants";

$ROW = $MyIconn->query($QUERY);
$form = new mysql_formgen($ROW);
//format the form url
echo '<!--START PLANT INSERT FORM -->
<form enctype="multipart/form-data" method="POST" action="?context=Pinsert">
<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />';
$form->display_add_form();
//end the form
echo '<input type="submit" value="Submit" name="goins">
<input type="reset" value="Reset" name="resetresrc">
</form>
<!--END PLANT INSERT FORM --> ';















$smarty->display(''.$theme_path.'/contentbg_footer.tpl'); 
}


//insert plant
if($Gcontext == 'Pinsert'){

$smarty->display(''.$theme_path.'/contentbg_header.tpl');
  
//format post vars
$Ptid       = $_POST["add_plant_tagID"];
$Pname      = $_POST["add_name"];
$Page       = $_POST["add_age"];
$Pheight    = $_POST["add_height"];
$Ploc       = $_POST["add_location"];
$Pgen       = $_POST["add_generation"];
$Pstat      = $_POST["add_status"];
$Pimg       = $_POST["add_image[]"];
$Pimg2      = $_FILES['add_image'];
$Ppic       =  addslashes (@file_get_contents($_FILES['add_image']['tmp_name']));
$Pnts       = $_POST["add_notes"];
$Pdna_inh   = $_POST["add_dna_inheritance"];
$Pdna_gen   = $_POST["add_dna_gender"];
$Pstrain    = $_POST["add_plant_strain"];

//EMPTY FIELD CHECKS
//mandatory
if(empty($Ptid) || empty($Pname) || empty($Page) || empty($Pheight) || empty($Pgen) || empty($Pstrain)){
    //echo 'Please go back and fill out the field marked with a *<a href="javascript:history.go(-1);">go back</a><br>';
        $msg = '<strong>ERROR! Please go back and fill out the fields marked with an (*) asterisk <a href="javascript:history.go(-1);">click here to go back</a><br></strong>';
        notice_msg($msg,error);

}else{

//FORMAT IMAGE upload SETTINGS FOR STORAGE
if(isset($_FILES['add_image'])) {

    if(is_uploaded_file($_FILES['add_image']['tmp_name'])) {
        // prepare the image for insertion
            $imgData = addslashes (file_get_contents($_FILES['add_image']['tmp_name']));   
        // get the image info
          $size = getimagesize($_FILES['add_image']['tmp_name']);
          
        // our sql query
            $sql = "INSERT INTO cg_images
                ( name , image , type, size, plant_id)
                VALUES
                ('{$_FILES['add_image']['name']}', '$imgData', '{$size['mime']}', '{$size[3]}', '{$Ptid}' )";
                
           // echo 'SQL STMT: '.$size['mime'].'<BR> '.$size[3].'<br>'.$_FILES['add_image']['name'].'<br>';
        // insert the image
        if(!mysql_query($sql)) {
                    $msg = '<strong>Unable to upload image <a href="javascript:history.go(-1);">go back</a><br></strong>';
                    notice_msg($msg,error);
            echo mysql_error();echo'<br>';
            echo mysql_errno();echo'<br>';
            }
     
        }  
     }

     

$PIsql = 'INSERT INTO `cg_plants` ( `plant_tagID` , `name` , `age` , `height` , `location` , `generation` , `status` , `image` , `notes` , `dna_inheritance` , `dna_gender` , `plant_strain` , `date_added` ) '
        . ' VALUES (
                    "'.$Ptid.'",
                    "'.$Pname.'",
                    "'.$Page.'",
                    "'.$Pheight.'",
                    "'.$Ploc.'",
                    "'.$Pgen.'",
                    "'.$Pstat.'",
                    "'.$Ppic.'",
                    "'.$Pnts.'",
                    "'.$Pdna_inh.'",
                    "'.$Pdna_gen.'",
                    "'.$Pstrain.'",
                    "'.$alld.'");';
                    
                    
                         
if(mysql_query($PIsql)) {

$msg = '<strong>Successfully added new plant!</strong><br>
Tag id: '.$Ptid.'<br>Name: '.$Pname.'<br>
Age: '.$Page.'<br> 
Height: '.$Pheight.' inches<br>
Location: '.$Ploc.'<br>
Generation: #.'.$Pgen.'<br>
Status: '.$Pstat.'<br>
DNA INH: '.$Pdna_inh.'<br>
DNA Gender: '.$Pdna_gen.'<br>
Strain: '.$Pstrain.'<br>
Click <a href="plants.php">here</a> to continue<br>';

notice_msg($msg,info);



}else{
            echo 'Unable to add new plant, please try again later.';
            echo mysql_error();echo'<br>';
            echo mysql_errno();echo'<br>';
}


} 






$smarty->display(''.$theme_path.'/contentbg_footer.tpl');
}


if($Gcontext == 'Pdelete' && $Gprompt != "yes"){
$msg = '<strong>Deleting plant record #'.$Plid.'</strong><br>Are you sure you want to continue?<br>
<a href="?context=Pdelete&prompt=yes&PLID='.$Plid.'">yes</a>|<a href="plants.php">no</a>';
notice_msg($msg,error);
}else if($Gcontext == 'Pdelete' && $Gprompt == "yes"){
    
    //delete account
    $del_query = "DELETE from cg_plants WHERE ID = '$Plid'";
    $del_s = mysql_query($del_query) or die(mysql_error("can not delete plant"));

if($del_s){
 $msg = '<strong>Deleted plant record #'.$Plid.'</strong><br>Redirecting in 2 seconds.... 
 <meta http-equiv="Refresh" content="2; URL=plants.php">';
notice_msg($msg,info);
}else{
 $msg = '<strong>ERROR deleting plant record #'.$Plid.'!</strong><br>';
notice_msg($msg,error);
}

   
}


if($Gcontext == 'Pview'){
$msg = '<strong>Plant #'.$Plid.' Overview</strong>';
notice_msg($msg,info);
}

include(dirname(__FILE__).'/footer.php');
}
?>
