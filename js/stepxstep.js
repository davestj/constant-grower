 var prevLink = '<a class="prev" href="#">< < Prev</a>';
        var nextLink = '</a><a class="next" href="#">Next >></a>';
        var navHTML = '<div class="prev-next">' +
                         prevLink +
                         nextLink +
                      '</div>';
        $(function(){
            // init
            $('#setup_wizard_form > div')
                .hide()
                .append(navHTML);
            $('#first-step .prev').remove();
            $('#last-step .next').remove();
 
            // show first step
            $('#first-step').show();
 
            $('a.next').button().click(function(){
                var whichStep = $(this).parent().parent().attr('id');
 
                if( whichStep == 'first-step' )
                {
                    // validate first-step
                }
                else if( whichStep == 'second-step' )
                {
                    // validate second-step
                }
                else if( whichStep == 'last-step' )
                {
                    // validate last-step
                }
 
                $(this).parent().parent().hide().next().show();
            });
 
            $('a.prev').button().click(function(){
                $(this).parent().parent().hide().prev().show();
            });
			
			
			
			$( "#submit-steps" )
    		.button().click(function() {
    		$('#setup_wizard_form').submit();
            });

			
			
			
			 
        });