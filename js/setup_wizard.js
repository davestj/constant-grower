// JavaScript Document
$(function() {
        // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
        $( "#dialog:ui-dialog" ).dialog( "destroy" );
        
        var assetID = $( "#assetID" ),
            itemName = $( "#itemName" ),
            allFields = $( [] ).add( assetID ).add( itemName ),
            tips = $( ".validateTips" );

function updateTips( t ) {
            tips
                .text( t )
                .addClass( "ui-state-highlight" );
            setTimeout(function() {
                tips.removeClass( "ui-state-highlight", 1500 );
            }, 500 );
        }


function checkLength( o, n, min, max ) {
            if ( o.val().length > max || o.val().length < min ) {
                o.addClass( "ui-state-error" );
                updateTips( "Length of " + n + " must be between " +
                    min + " and " + max + "." );
                return false;
            } else {
                return true;
            }
        }

        function checkRegexp( o, regexp, n ) {
            if ( !( regexp.test( o.val() ) ) ) {
                o.addClass( "ui-state-error" );
                updateTips( n );
                return false;
            } else {
                return true;
            }
        }
        


//DIALOGS
//ASSETS           
$('#asset-form').dialog({
    autoOpen: false,
    width: 400,
    height: 400,
    buttons: {
    
        "Ok": function() {
        $( "#assets tbody" ).append( "<tr>" +
                                    "<td>" + assetID.val() + "</td>" + 
                                    "<td>" + itemName.val() + "</td>" +
                                    "</tr>"  
                                     ); 
                        $( this ).dialog( "close" );
         
        }, 
        "Cancel": function() { 
        $(this).dialog("close"); 
        } 
    }
});   

//plants           
$('#plant-form').dialog({
    autoOpen: false,
    width: 600,
    height: 600,
    buttons: {
        "Ok": function() { 
        $(this).dialog("close"); 
        }, 
        "Cancel": function() { 
        $(this).dialog("close"); 
        } 
    }
}); 

//GROW ROOMS           
$('#gr-form').dialog({
    autoOpen: false,
    width: 600,
    height: 600,
    buttons: {
        "Ok": function() { 
        $(this).dialog("close"); 
        }, 
        "Cancel": function() { 
        $(this).dialog("close"); 
        } 
    }
}); 

       
//BUTTONS        
$( "#create-asset" )
    .button().click(function() {
    $( "#asset-form" ).dialog( "open" );
            });
            
$( "#create-plant" )
    .button().click(function() {
    $( "#plant-form" ).dialog( "open" );
            });
                        
$( "#create-gr" )
    .button().click(function() {
    $( "#gr-form" ).dialog( "open" );
            });            
$( "#create-clone" )
    .button().click(function() {
    $( "#clone-form" ).dialog( "open" );
            });            
$( "#next-step" )
    .button().click(function() {
    $('#asset_wizard').submit();
            });             
            
            
            
            
            
    });  