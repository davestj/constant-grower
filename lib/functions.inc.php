<?php
/**
* @name: function lib
* @author: dstjohn
* @copyright: 2011 davestj@gmail.com
* @version: 1.0
*/


//google auth functions
require_once 'Zend/Loader.php';
Zend_Loader::loadClass('Zend_Gdata');
Zend_Loader::loadClass('Zend_Gdata_AuthSub');
Zend_Loader::loadClass('Zend_Gdata_ClientLogin');
Zend_Loader::loadClass('Zend_Gdata_Calendar');


function getClientLoginHttpClient($user, $pass) 
{
  $service = Zend_Gdata_Calendar::AUTH_SERVICE_NAME;

  $client = Zend_Gdata_ClientLogin::getHttpClient($user, $pass, $service);
  return $client;
}


function parse_date($date)
{
    if (preg_match('/([0-9]{2,4})-([0-9][0-9])-([0-9][0-9])T([0-9][0-9]):([0-9][0-9]):([0-9][0-9])(\.[0-9][0-9])?Z/i', $date, $matches))
    {
        if (isset($matches[7]) && substr($matches[7], 1) >= 50)
            $matches[6]++;
        return strtotime("$matches[1]-$matches[2]-$matches[3] $matches[4]:$matches[5]:$matches[6] -0000");
    }
    else if (preg_match('/([0-9]{2,4})-([0-9][0-9])-([0-9][0-9])T([0-9][0-9]):([0-9][0-9]):([0-9][0-9])(\.[0-9][0-9])?(\+|-)([0-9][0-9]):([0-9][0-9])/i', $date, $matches))
    {
        if (isset($matches[7]) && substr($matches[7], 1) >= 50)
            $matches[6]++;
        return strtotime("$matches[1]-$matches[2]-$matches[3] $matches[4]:$matches[5]:$matches[6] $matches[8]$matches[9]$matches[10]");
    }
    else
    {
        return strtotime($date);
    }
}


//debug funcs
function parse_class($obj){
    $the_class = get_class($obj);
  echo "<br><pre>Parsing Class: ".$the_class."<br>";

  $the_method  = get_class_methods($obj);
  echo "<strong><i><b>Class Methods</b></i></strong><br>";
  
  foreach ($the_method as $method_name) {
    $parsed_method = "$the_class::$method_name";
    echo "$parsed_method<br>";
    }
     
  echo "</pre><br>";
    
}



//enumerate mysql fields
function get_Enumerated_Values($table,$field,$sorted=true)
{


    $result=mysql_query('show columns from '.$table.';');
    while($tuple=mysql_fetch_assoc($result))
    {
        if($tuple['Field'] == $field)
        {
            $types=$tuple['Type'];
            $beginStr=strpos($types,"(")+1;
            $endStr=strpos($types,")");
            $types=substr($types,$beginStr,$endStr-$beginStr);
            $types=str_replace("'","",$types);
            $types=preg_split(',',$types);
            if($sorted)
                sort($types);
            break;
        }
    }

    return($types);
}


function is_ip($ip){

if (preg_match('/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/', $ip)){
return true;
}

return false;
}



function notice_msg($themessage,$type){
    
    if($type == error){
    $html = '<div class="ui-widget">
                <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"> 
                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>'.$themessage.'</p>
            </div>
        </div><br>';
    print $html;
    
    }elseif($type == info){
     $html = '<div class="ui-widget">
            <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>'.$themessage.'</p>
            </div>
        </div><br>';   
     print $html;   
    }else{
    $html = '0';
    return $html;
    }
    
    
    
    
}



//query generator
function mysql_generate_vars($tbl,$prefix){
    
$qColumnNames = mysql_query("SHOW COLUMNS FROM $tbl") or die("mysql error");
$numColumns = mysql_num_rows($qColumnNames);
$x = 0;
$pfx = $prefix;
while ($x < $numColumns)
{
    $colname = mysql_fetch_row($qColumnNames);
   // $col = "$$colname[0] = $result [ \"$colname[0] \ "\];\n";
    $col[$colname[0]] = $colname[0];
    
    $data = "\$$pfx$colname[0]  = \$result[\"$colname[0]\"];<br>";
    $x++;
    print $data;
}

}



//mysql form generator
function mysql_formgen_debug($tbl){

    $result = mysql_query("SELECT * FROM $tbl");
$fields = mysql_num_fields($result);
$rows   = mysql_num_rows($result);
$table  = mysql_field_table($result, 0);
echo "Your '" . $table . "' table has " . $fields . " fields and " . $rows . " record(s)<br>";
echo "The table has the following fields:<br>";
for ($i=0; $i < $fields; $i++) {
    $type  = mysql_field_type($result, $i);
    $name  = mysql_field_name($result, $i);
    $len   = mysql_field_len($result, $i);
    $flags = mysql_field_flags($result, $i);
    echo $type . " " . $name . " " . $len . " " . $flags . "<br>";
}
mysql_free_result($result);



    
}






/*
       NOT_NULL_FLAG = 1                                                                             
       PRI_KEY_FLAG = 2                                                                              
       UNIQUE_KEY_FLAG = 4                                                                           
       BLOB_FLAG = 16                                                                                
       UNSIGNED_FLAG = 32                                                                            
       ZEROFILL_FLAG = 64                                                                            
       BINARY_FLAG = 128                                                                             
       ENUM_FLAG = 256                                                                               
       AUTO_INCREMENT_FLAG = 512                                                                     
       TIMESTAMP_FLAG = 1024                                                                         
       SET_FLAG = 2048                                                                               
       NUM_FLAG = 32768                                                                              
       PART_KEY_FLAG = 16384                                                                         
       GROUP_FLAG = 32768                                                                            
       UNIQUE_FLAG = 65536
       
numerics
-------------
BIT: 16
TINYINT: 1
BOOL: 1
SMALLINT: 2
MEDIUMINT: 9
INTEGER: 3
BIGINT: 8
SERIAL: 8
FLOAT: 4
DOUBLE: 5
DECIMAL: 246
NUMERIC: 246
FIXED: 246

dates
------------
DATE: 10
DATETIME: 12
TIMESTAMP: 7
TIME: 11
YEAR: 13

strings & binary
------------
CHAR: 254
VARCHAR: 253
ENUM: 254
SET: 254
BINARY: 254
VARBINARY: 253
TINYBLOB: 252
BLOB: 252
MEDIUMBLOB: 252
TINYTEXT: 252
TEXT: 252
MEDIUMTEXT: 252
LONGTEXT: 252
*/ 
class mysql_formgen implements Iterator
            {
               
                private $result;
                private $position;
                private $row;
               
               
                function __construct($result){
                    $this->result = $result;
                    $this->position = 0;
                    $this->rewind();    
                }
               
                public function current(){
                    return $this->row;
                }
               
                public function next(){
                    $this->position++;
                    $this->row = $this->result->fetch_field();   
                }
               
                public function valid(){
                    return (boolean) $this->row;
                }
               
                public function key(){
                    return $this->position;
                }
               
                public function rewind(){
                    $this->position = 0;
                    $this->result->field_seek(0);
                    $this->next();
                }
               
                //    This function show data in table
                public function display_add_form(){
                   
                    echo '<table id="db_table_info">';
                    echo '<tr>
                      <th>Options</th>
                      <th></th>
                       <th></th>
                       </tr>
                       ';
                    while($this->valid()){
                        echo '<tr>';
                        $Tname          = $this->current()->name;
                        $Torgname       = $this->current()->orgname;
                        $Torgtbl        = $this->current()->orgtable;
                        $Tdef           = $this->current()->def;
                        $Ttype          = $this->current()->type;
                        $Tcurlength     = $this->current()->length;
                        $Tmaxlength     = $this->current()->max_length;
                        $Tcharset       = $this->current()->charsetnr; 
                        $Tflags         = $this->current()->flags;
                        
                        if($Ttype == 252 && $Tcharset != 8){
                          $Tftype = '<input type="file" name="add_'.$Torgname.'" size="20">Max Upload: 8Mb';  
                        }
                        else if($Ttype == 252 && $Tcharset == 8){
                          $Tftype = '<textarea rows="8" name="add_'.$Torgname.'" cols="38"></textarea>';  
                        }
                        else if($Ttype == 253){
                          $Tftype = '<input type="text" name="add_'.$Torgname.'" size="20">(*)';  
                        }
                        else if($Ttype == 254){
                          $Tftype = '<select size="1" name="add_'.$Torgname.'">';
                          
                         
                         
                         
  $result = mysql_query('show columns from '.$Torgtbl.';');
    while($tuple = mysql_fetch_assoc($result))
    {
        if($tuple['Field'] == $Torgname)
        {
            $types      =   $tuple['Type'];
            $beginStr   =   strpos($types,"(")+1;
            $endStr     =   strpos($types,")");
            $types      =   substr($types,$beginStr,$endStr-$beginStr);
            $types      =   str_replace("'","",$types);
            $types      =   @split(',',$types);
            
            if($sorted){
                sort($types);
            }
           // break;
        }
    } 
              foreach ($types as $options){
                    $Tftype .= '<option value="'.$options.'">'.$options.'</option>
                    ';
                    }
                      
                             
                          $Tftype .= '</select>
                          ';
                            
                        }
                        else if($Ttype == 3){
                          $Tftype = '<input type="hidden" name="add_'.$Torgname.'">';  
                        }
                        
                        
                        printf("\n\t<td align=\"right\">%s:</td>\n", $Tname);
                        printf("\t<td></td>\n",   $Torgname);
                       // printf("\t<td>%s</td>\n",   $Torgtbl); 
                       // printf("\t<td>%s</td>\n",   $Tdef);
                       // printf("\t<td>%s</td>\n",   $Tmaxlength);
                       // printf("\t<td>%s</td>\n",   $Tcurlength);
                       // printf("\t<td>%s</td>\n",   $Tflags);
                        printf("\t<td>%s</td>\n",  $Tftype);
                       // printf("\t<td>%s</td>\n",   $Tcharset);

                        echo '</tr>
                        ';       
           
                        $this->next();
                    }
                   
                    echo '</table>';
                }
            }
















 
 function pagination($query, $per_page = 10,$page = 1, $url = '?'){        
        $query = "SELECT COUNT(*) as `num` FROM {$query}";
        $row = mysql_fetch_array(mysql_query($query));
        $total = $row['num'];
        $adjacents = "2"; 

        $page = ($page == 0 ? 1 : $page);  
        $start = ($page - 1) * $per_page;                                
        
        $prev = $page - 1;                            
        $next = $page + 1;
        $lastpage = ceil($total/$per_page);
        $lpm1 = $lastpage - 1;
        
        $pagination = "";
        if($lastpage > 1)
        {    
            $pagination .= "<ul class='pagination'>";
                    $pagination .= "<li class='details'>Page $page of $lastpage</li>";
            if ($lastpage < 7 + ($adjacents * 2))
            {    
                for ($counter = 1; $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>$counter</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page=$counter'>$counter</a></li>";                    
                }
            }
            elseif($lastpage > 5 + ($adjacents * 2))
            {
                if($page < 1 + ($adjacents * 2))        
                {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='{$url}page=$counter'>$counter</a></li>";                    
                    }
                    $pagination.= "<li class='dot'>...</li>";
                    $pagination.= "<li><a href='{$url}page=$lpm1'>$lpm1</a></li>";
                    $pagination.= "<li><a href='{$url}page=$lastpage'>$lastpage</a></li>";        
                }
                elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
                {
                    $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                    $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                    $pagination.= "<li class='dot'>...</li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='{$url}page=$counter'>$counter</a></li>";                    
                    }
                    $pagination.= "<li class='dot'>..</li>";
                    $pagination.= "<li><a href='{$url}page=$lpm1'>$lpm1</a></li>";
                    $pagination.= "<li><a href='{$url}page=$lastpage'>$lastpage</a></li>";        
                }
                else
                {
                    $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                    $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                    $pagination.= "<li class='dot'>..</li>";
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='current'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='{$url}page=$counter'>$counter</a></li>";                    
                    }
                }
            }
            
            if ($page < $counter - 1){ 
                $pagination.= "<li><a href='{$url}page=$next'>Next</a></li>";
                $pagination.= "<li><a href='{$url}page=$lastpage'>Last</a></li>";
            }else{
                $pagination.= "<li><a class='current'>Next</a></li>";
                $pagination.= "<li><a class='current'>Last</a></li>";
            }
            $pagination.= "</ul>\n";        
        }
    
    
        return $pagination;
    } 
?>
