<?php
$groom_ID = $result["ID"];
$groom_name = $result["name"];
$groom_location = $result["location"];
$groom_type = $result["type"];
$groom_humidity = $result["humidity"];
$groom_temp = $result["temp"];
$groom_date_built = $result["date_built"];
$groom_tags = $result["tags"];
$groom_power_type = $result["power_type"];
$groom_power_amps = $result["power_amps"];
$groom_power_sockets = $result["power_sockets"];
$groom_power_usage = $result["power_usage"];
$groom_date_added = $result["date_added"];

?>