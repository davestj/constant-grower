<?php
$plant_ID            = $result["ID"];
$plant_tagID         = $result["plant_tagID"]; 
$plant_NAME          = $result["name"]; 
$plant_AGE           = $result["age"];
$plant_HT            = $result["height"]; 
$plant_LOC           = $result["location"]; 
$plant_GEN           = $result["generation"];
$plant_STATUS        = $result["status"]; 
$plant_IMG           = $result["image"]; 
$plant_NOTE          = $result["notes"]; 
$plant_DNAINH        = $result["dna_inheritance"]; 
$plant_DNAGEN        = $result["dna_gender"]; 
$plant_NPH           = $result["nute_ph"]; 
$plant_NPPM          = $result["nute_ppm"]; 
$plant_NTEMP         = $result["nute_temp"]; 
$plant_NDATE         = $result["nute_date"]; 
$plant_STRAIN        = $result["plant_strain"]; 
$plant_DADDED        = $result["date_added"]; 
$plant_DPLANTED      = $result["date_planted"]; 
$plant_DFLSHD        = $result["date_flushed"]; 
$plant_DCLOND        = $result["date_cloned"]; 
$plant_DFLWRD        = $result["date_flowered"]; 
$plant_DHRVST        = $result["date_harvested"]; 
$plant_DPACK         = $result["date_packaged"]; 
$plant_DAYV          = $result["days_in_veg"]; 
$plant_DAYF          = $result["days_in_flower"]; 
$plant_DAYC          = $result["days_in_cure"]; 

?>