<?php
/**
* @name: config file
* @author: dstjohn
* @copyright: 2011 davestj@gmail.com
* @version: 1.0
*/

//general config settings
//misc
$timer_start = str_replace(" ","",microtime());
$currentday = date ( "l" );
$currentdate = date ( "j" );
$currentmonth = date ( "F" );
$currentyear = date ( "Y" );
$currenttime = date ( "h:i:s A" );

$alld = date ( "l - F - j - Y - h:i:s A" );
$zero_month_format = date ( "m/d/Y" );

$DOCUMENT_ROOT = apache_getenv("DOCUMENT_ROOT");

//theme settings
$theme_name  = 'constant_green'; //others = pastel_blood, pastel_sunshine,pastel_grass, pastel_blue_sky
$themes_root = ''.$DOCUMENT_ROOT.'/themes';
$theme_path  = ''.$themes_root.'/'.$theme_name.'';
$theme_url   = 'themes/'.$theme_name.'';

//google api settings
$SA_gcal_user = '';
$SA_gcal_pass = '';

//mysql connection

//localserver
$MYsrv_host = 'localhost';
$MYsrv_user = 'root';
$MYsrv_pass = '';
$MYsrv_name = 'constant_grower';


//initialize custom library paths
ini_set('include_path','/usr/home/constantgrower.com/smarty/libs:/usr/home/constantgrower.com/zend/library');
ini_set("session.save_path", "$DOCUMENT_ROOT/../sesstmp"); 

?>
