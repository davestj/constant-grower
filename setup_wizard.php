<?php
require_once(dirname(__FILE__).'/lib/config.inc.php');

session_start();
if($_SESSION['LOGIN_STATUS'] != "OK"){
    session_unset();
    session_destroy();
    echo 'You are not logged in, redirecting<meta http-equiv="Refresh" content="1; URL=login.php">';
}else{
include(dirname(__FILE__).'/header.php');

    $msg = '<strong>Setup Wizard</strong><br>';
notice_msg($msg,info);

$cnt_hdr = '
<!-- START CONTENT HEADER --><br><br>
<table id="Table_01" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="18" height="18">
            <img src="themes/constant_green/images/page_contentbg/form_content_page_toplft.png" width="18" height="18" alt="" /></td>
        <td width="100%" height="18" background="themes/constant_green/images/page_contentbg/form_content_page_top.png"></td>
        <td width="18" height="18">
            <img src="themes/constant_green/images/page_contentbg/form_content_page_toprt.png" width="18" height="18" alt="" /></td>
    </tr>
    <tr>
        <td width="18" height="100%" background="themes/constant_green/images/page_contentbg/form_content_page_leftside.png">
            &nbsp;</td>
        <td width="100%" height="100%" bgcolor="#FBF6D7" valign="top">
        <!--END CONTENT HEADER -->';
$cnt_ftr = '
         <!--START CONTENT FOOTER -->    
            </td>
        <td width="18" height="100%" background="themes/constant_green/images/page_contentbg/form_content_page_rtside.png">
            &nbsp;</td>
    </tr>
    <tr>
        <td width="18" height="19">
            <img src="themes/constant_green/images/page_contentbg/form_content_page_btmlft.png" width="18" height="19" alt="" /></td>
        <td width="100%" height="19" background="themes/constant_green/images/page_contentbg/form_content_page_btm.png"></td>
        <td width="18" height="19">
            <img src="themes/constant_green/images/page_contentbg/form_content_page_btmrt.png" width="18" height="19" alt="" /></td>
    </tr>
</table>
<br><br>
<br><br>
<br><br>
<br><br>
<!-- END CONTENT FOOTER--> 
';


if($Gcontext == "step1"){
    
$smarty->display(''.$theme_path.'/page_frame_header.tpl');



     
    echo '

<p>Please follow each step and fill in all data.</p>
<form id="setup_wizard_form" method="post" action="setup_wizard.php?context=finish_wizard">
 
 <div id="first-step">
<img border="0" src="themes/constant_green/images/buttons/step1-growroom.png" width="257" height="52">
'.$cnt_hdr.'
<div>
<table border="0" width="100%" cellspacing="3" cellpadding="0">
    <tr>
        <td width="174" align="right">Name:</td>
        <td><input type="text" name="gr_name" size="40"></td>
    </tr>
    <tr>
        <td width="174" align="right">Location:</td>
        <td><input type="text" name="gr_location" size="40"></td>
    </tr>
    <tr>
        <td width="174" align="right">Type:</td>
        <td><select size="1" name="gr_type">
        <option selected value="pick">Choose</option>
';
              $opts = get_Enumerated_Values('cg_rooms','type');
                    foreach ($opts as $options){
                    echo '<option value="'.$options.'">'.$options.'</option>';
                    }
    echo '
        </select></td>
    </tr>
    <tr>
        <td width="174" align="right"></td>
        <td></td>
    </tr>
</table>
</div> 
'.$cnt_ftr.'  
</div>
<!-- end first-step -->

<!-- start second-step -->   
<div id="second-step">
    <img border="0" src="themes/constant_green/images/buttons/step2-asset.png" width="257" height="52">
'.$cnt_hdr.' 
        <div>       
 <table border="0" width="100%" cellspacing="3" cellpadding="0">
    <tr>
        <td width="174" align="right">Asset Name:</td>
        <td><input type="text" name="as_name" size="40"></td>
    </tr>
    <tr>
        <td width="174" align="right">Location:</td>
        <td><input type="text" name="as_location" size="40"></td>
    </tr>
    <tr>
        <td width="174" align="right">Type:</td>
        <td><select size="1" name="as_type">
        <option selected value="pick">Choose</option>
 ';
              $opts = get_Enumerated_Values('cg_assets','type');
                    foreach ($opts as $options){
                    echo '<option value="'.$options.'">'.$options.'</option>';
                    }
    echo '



        </select></td>
    </tr>
    <tr>
        <td width="174" align="right"></td>
        <td></td>
    </tr>
</table>       
        
        
        '.$cnt_ftr.'
        </div>

</div>
<!-- end second-step -->







 
<div id="last-step">
    <img border="0" src="themes/constant_green/images/buttons/step3-finish-addplant.png" width="257" height="52">
    '.$cnt_hdr.' 
    <div>
 
<table border="0" width="100%" cellspacing="3" cellpadding="0">
    <tr>
        <td width="174" align="right">Plant Name:</td>
        <td><input type="text" name="pl_name" size="40"></td>
    </tr>
    <tr>
        <td width="174" align="right">Plant Age:</td>
        <td><input type="text" name="pl_age" size="40"></td>
    </tr>
    <tr>
        <td width="174" align="right">Location:</td>
        <td><select size="1" name="pl_location">
        <option selected value="pick">Choose</option>
  ';
              $opts = get_Enumerated_Values('cg_plants','location');
                    foreach ($opts as $options){
                    echo '<option value="'.$options.'">'.$options.'</option>';
                    }
    echo '



        </select></td>
    </tr>
    <tr>
        <td width="174" align="right"></td>
        <td><input type="submit" name="swiz_submit" id="submit-steps" value="Submit & Finalize" /></td>
    </tr>
</table>
    '.$cnt_ftr.'
    </div> 
</div> 
<!-- end last-step -->
 
</form>
    
    ';
    
$smarty->display(''.$theme_path.'/page_frame_footer.tpl');    






}
if($Gcontext == "finish_wizard"){
    
//$smarty->display(''.$theme_path.'/page_frame_header.tpl');


      
$grname     = $_POST["gr_name"];
$grloc      = $_POST["gr_location"];
$grtype     = $_POST["gr_type"];
$asname     = $_POST["as_name"];
$asloc      = $_POST["as_location"];
$astype     = $_POST["as_type"];
$plname     = $_POST["pl_name"];
$plage      = $_POST["pl_age"];
$pl_loc     = $_POST["pl_location"]; 
 
//grow room query
$TQ1 = 'INSERT INTO `cg_rooms` ( `name`, `location`, `type`) '
        . ' VALUES (    "'.$grname.'",
                        "'.$grloc.'",
                        "'.$grtype.'");';
                        
//asset query
$TQ2 = 'INSERT INTO `cg_assets` ( `name`, `location`, `type`) '
        . ' VALUES (    "'.$asname.'",
                        "'.$asloc.'",
                        "'.$astype.'");';
//plant query
$TQ3 = 'INSERT INTO `cg_plants` ( `name`, `age`, `location`) '
        . ' VALUES (    "'.$plname.'",
                        "'.$plage.'",
                        "'.$pl_loc.'");';

//verify mysql insertion
if (mysql_query($TQ1) && mysql_query($TQ2) && mysql_query($TQ3)) {
    echo ''.$cnt_hdr.'<h2>Setup Results</h2>
    Grow room name: '.$grname.'<br>
    Grow room location: '.$grloc.' <br>
    Grow room type: '.$grtype .'<br>
    Asset name: '.$asname.'<br>
    Asset type: '.$astype.'<br>
    Asset location: '.$asloc.'<br>
    Plant name: '.$plname.'<br>
    Plant age: '.$plage.'<br>
    Plant location: '.$pl_loc.'<br>
    Congradulations, click <a href="dashboard.php">here</a> to start using Constant Grower 
    '.$cnt_ftr.'';
     }else{
         echo mysql_error();echo'<br>';
         echo mysql_errno();echo'<br>';
         echo'Please try again, Database is buisy<br>';
     }
       

    
    
    
    
//$smarty->display(''.$theme_path.'/page_frame_footer.tpl'); 

    
}


include(dirname(__FILE__).'/footer.php');
}
?>