<?php
require_once(dirname(__FILE__).'/lib/config.inc.php');

session_start();
if($_SESSION['LOGIN_STATUS'] != "OK"){
    session_unset();
    session_destroy();
    echo 'You are not logged in, redirecting<meta http-equiv="Refresh" content="1; URL=login.php">';
}else{
include(dirname(__FILE__).'/header.php');
echo '<br>';

if($gr_check[0] < 1){
$msg = '<strong>Alert:</strong> No grow rooms have been defined, please run <a href="setup_wizard.php?context=step1">setup wizard</a><br>
or <a href="grow_rooms.php?context=addroom">click here</a> to add a plant manually.';  
notice_msg($msg,error);
}




include(dirname(__FILE__).'/footer.php');
}
?>