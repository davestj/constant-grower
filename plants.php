<?php
require_once(dirname(__FILE__).'/lib/config.inc.php');

session_start();
if($_SESSION['LOGIN_STATUS'] != "OK"){
    session_unset();
    session_destroy();
    echo 'You are not logged in, redirecting<meta http-equiv="Refresh" content="1; URL=login.php">';
}else{
include(dirname(__FILE__).'/header.php');
//include(dirname(__FILE__).'/lib/ps_pagination.php'); 
echo '<br>';
$smarty->assign('page_title','Plant List');






if($Tplant_check[0] <  1){
$msg = '<strong>Alert:</strong> No plants have been defined, please run <a href="setup_wizard.php?context=step1">setup wizard</a><br>
                or <a href="plants.php?context=addplant">click here</a> to add a plant manually.';
notice_msg($msg,error);

 
}else{

        
if($Gsort_mode == 'ASC'){
     $state_SET = "DESC";
     $img_src = 'themes/constant_green/images/data_grid_button/data_grid_button_sort_arrow_up.png';
}
if($Gsort_mode == 'DESC'){
     $state_SET = "ASC";
     $img_src = 'themes/constant_green/images/data_grid_button/data_grid_button_sort_arrow.png';
}
        
if(!isset($Gsort_mode)){
    $state_SET = "ASC";
    $img_src = 'themes/constant_green/images/data_grid_button/data_grid_button_sort_arrow.png';
}
if(!isset($Gsort)){
$Gsort = 'ID';
} 
//display the plants
//SETUP PAGINATION
        $page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
        $limit = 4;
        $startpoint = ($page * $limit) - $limit;
        //to make pagination
        $statement = "cg_plants";
        
        
        
 //show pag nav bar at top
 // echo pagination($statement,$limit,$page);
echo '
<br>
<div><a href="plant_manager.php?context=Padd">
<img border="0" src="themes/constant_green/images/buttons/add_plant_button.png" width="180" height="50"></a></div>
<br>
<!--START LIST VIEW COLUMN HEADER -->
<table border="0"  cellspacing="0" cellpadding="0">
    <tr>
        <td width="1">
        
        
<!-- START ACTION LV -->
<table id="Table_01" width="151" height="50" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="4" height="50" rowspan="2">
            <img src="themes/constant_green/images/data_grid_button/data_grid_button_leftside.png" width="4" height="50" alt="" /></td>
        <td width="132" height="50" rowspan="2" background="themes/constant_green/images/data_grid_button/data_grid_button_middle.png">
            <p align="center" class="grid_header_text"><b><font face="Verdana" size="4" color="#FFFFFF">
            <a href="plants.php?context=list&sort=ID&sort_mode='.$state_SET.'"> <font color="#FFFFFF">Action</font></a></font></b></td>
        <td width="15" height="39">
            <img src="themes/constant_green/images/data_grid_button/data_grid_button_rightside.png" width="15" height="39" alt="" /></td>
    </tr>
    <tr>
        <td width="15" height="11">
            <a href="plants.php?context=list&sort=ID&sort_mode='.$state_SET.'">
            <img src="'.$img_src.'" width="15" height="11" alt="" border="0" /></a></td>
    </tr>
</table>
<!-- END ACTION LV -->
                                                     
                                                     

        </td>
        
        <td width="1">
<!-- START name LV -->
<table id="Table_01" width="151" height="50" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="4" height="50" rowspan="2">
            <img src="themes/constant_green/images/data_grid_button/data_grid_button_leftside.png" width="4" height="50" alt="" /></td>
        <td width="132" height="50" rowspan="2" background="themes/constant_green/images/data_grid_button/data_grid_button_middle.png">
            <p align="center" class="grid_header_text"><b><font face="Verdana" size="4" color="#FFFFFF">
            <a href="plants.php?context=list&sort=name&sort_mode='.$state_SET.'"><font color="#FFFFFF">Name</font></a></font></b></td>
        <td width="15" height="39">
            <img src="themes/constant_green/images/data_grid_button/data_grid_button_rightside.png" width="15" height="39" alt="" /></td>
    </tr>
    <tr>
        <td width="15" height="11">
            <a href="plants.php?context=list&sort=name&sort_mode='.$state_SET.' ">
            <img src="'.$img_src.'" width="15" height="11" alt="" border="0" /></a></td>
    </tr>
</table>
<!-- END name LV -->
        </td>
        <td width="1"><!-- START location LV -->
<table id="Table_01" width="151" height="50" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="4" height="50" rowspan="2">
            <img src="themes/constant_green/images/data_grid_button/data_grid_button_leftside.png" width="4" height="50" alt="" /></td>
        <td width="132" height="50" rowspan="2" background="themes/constant_green/images/data_grid_button/data_grid_button_middle.png">
            <p align="center" class="grid_header_text"><b><font face="Verdana" size="4" color="#FFFFFF">
            <a href="plants.php?context=list&sort=location&sort_mode='.$state_SET.' ">
            <font color="#FFFFFF">Location</font></a></font></b></td>
        <td width="15" height="39">
            <img src="themes/constant_green/images/data_grid_button/data_grid_button_rightside.png" width="15" height="39" alt="" /></td>
    </tr>
    <tr>
        <td width="15" height="11">
            <a href="plants.php?context=list&sort=location&sort_mode='.$state_SET.' ">
            <img src="'.$img_src.'" width="15" height="11" alt="" border="0" /></a></td>
    </tr>
</table>
<!-- END location LV --></td>
        <td width="1">
        <!-- START status LV -->
<table id="Table_01" width="151" height="50" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="4" height="50" rowspan="2">
            <img src="themes/constant_green/images/data_grid_button/data_grid_button_leftside.png" width="4" height="50" alt="" /></td>
        <td width="132" height="50" rowspan="2" background="themes/constant_green/images/data_grid_button/data_grid_button_middle.png">
            <p align="center" class="grid_header_text"><b><font face="Verdana" size="4" color="#FFFFFF">
            <a href="plants.php?context=list&sort=status&sort_mode='.$state_SET.' "><font color="#FFFFFF">Status</font></a></font></b></td>
        <td width="15" height="39">
            <img src="themes/constant_green/images/data_grid_button/data_grid_button_rightside.png" width="15" height="39" alt="" /></td>
    </tr>
    <tr>
        <td width="15" height="11">
            <a href="plants.php?context=list&sort=status&sort_mode='.$state_SET.' ">
            <img src="'.$img_src.'" width="15" height="11" alt="" border="0" /></a></td>
    </tr>
</table>
<!-- END status LV -->
        </td>
        <td>
        <!-- START gender LV -->
<table id="Table_01" width="151" height="50" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="4" height="50" rowspan="2">
            <img src="themes/constant_green/images/data_grid_button/data_grid_button_leftside.png" width="4" height="50" alt="" /></td>
        <td width="132" height="50" rowspan="2" background="themes/constant_green/images/data_grid_button/data_grid_button_middle.png">
            <p align="center" class="grid_header_text"><b><font face="Verdana" size="4" color="#FFFFFF">
            <a href="plants.php?context=list&sort=dna_gender&sort_mode='.$state_SET.' ">
            <font color="#FFFFFF">Gender</font></a></font></b></td>
        <td width="15" height="39">
            <img src="themes/constant_green/images/data_grid_button/data_grid_button_rightside.png" width="15" height="39" alt="" /></td>
    </tr>
    <tr>
        <td width="15" height="11">
            <a href="plants.php?context=list&sort=dna_gender&sort_mode='.$state_SET.' ">
            <img src="'.$img_src.'" width="15" height="11" alt="" border="0" /></a></td>
    </tr>
</table>
<!-- END gender LV -->
        </td>
    </tr>

';    


//show records
$query = mysql_query("SELECT * FROM {$statement} ORDER BY {$Gsort} {$Gsort_mode} LIMIT {$startpoint} , {$limit}");
//$query = mysql_query("SELECT * FROM cg_plants WHERE ID > 0 ORDER BY {$Gsort} {$Gsort_mode} LIMIT {$startpoint}, {$limit}"); 
            
while ($result = mysql_fetch_assoc($query)) {
    include(dirname(__FILE__).'/lib/plant_query.php');
    $count++;
    
    if(($count % 2)==0){
                   $bgcolor = "#eff8d9";
                   $rlover  = "#b7cf7b";
            } else {                
                $bgcolor = "#cdea89";
                $rlover  = "#defc97";
            }
            //onMouseOver=this.style.backgroundColor="'.$rlover.'"  onMouseOut=this.style.backgroundColor="" 
echo '
    <tr class="item_listing" bgcolor="'.$bgcolor.'">
        <td>
        <table border="0" width="100%" cellspacing="2" cellpadding="1">
        <tr>
            <td align="center">
            <a href="plant_manager.php?context=Pdelete&PLID='.$plant_ID.'">
            <img border="0" src="themes/constant_green/images/icons/actions/File-Delete-icon.png" width="26" height="26"></a></td>
            <td align="center">
            <a href="plant_manager.php?context=Pedit&PLID='.$plant_ID.'">
            <img border="0" src="themes/constant_green/images/icons/actions/Edit-Document-icon.png" width="26" height="26"></a></td>
            <td align="center">
            <a href="plant_manager.php?context=Pview&PLID='.$plant_ID.'">
            <img border="0" src="themes/constant_green/images/icons/actions/Preview-icon.png" width="26" height="26"></a></td>
        </tr>
        </table></td>
        <td bgcolor="'.$bgcolor.'">'.$plant_NAME.'</td>
        <td bgcolor="'.$bgcolor.'">'.$plant_LOC.'</td>
        <td bgcolor="'.$bgcolor.'">'.$plant_STATUS.'</td>
        <td bgcolor="'.$bgcolor.'">'.$plant_DNAGEN.'</td>
    </tr>
';
    }
        

echo '
</table>
<!--END LV COLUMN HEADER -->
<br><br>

';
 //show pag nav bar at bottom
 echo pagination($statement,$limit,$page);      
}//end plant display





include(dirname(__FILE__).'/footer.php');
}
?>
